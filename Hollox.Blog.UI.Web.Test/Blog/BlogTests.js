﻿describe("BlogService", function ()
{
    it('It should call GetStatisticsByYearMonth and return empty list if there is no statistic', function () {
        var result = 2;
        expect(result).toBe(2);
    });

    it('It should call GetStatisticsByYearMonth and return a listof statistics if there is any statistics', function () {
        var result = 2;

        /*
        2015
            12  4
        2016
             2   5
             3   2
             5   3

        */
        expect(result).toBe(2);
    });

    it('It should call GetPosts and return empty list if there is no post', function () {
        var result = 2;
        expect(result).toBe(2);
    });

    it('It should call GetPosts and return a list of posts if there is any statistics', function () {
        var result = 2;
        expect(result).toBe(2);
    });
});